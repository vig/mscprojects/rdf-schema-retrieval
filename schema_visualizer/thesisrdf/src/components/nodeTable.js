import React, { useEffect, useRef, useLayoutEffect, useState } from "react";
import {network, Network, Node} from "vis-network";
import {DataSet, Item} from 'vis-data';
//import { SchemaNode } from "./SchemaNode";
//import { SchemaEdge } from "./SchemaEdge";

//import hierarchyData from './hierarchyData.json'
//import dataDump1 from './dataDump1.json'
import dataDump2 from './datadump2.json'
import dataDump3 from './dataDump3.json'
import dataDump4 from './dataDump4.json'
import { click } from "@testing-library/user-event/dist/click";


let nodedump = dataDump3.slice(0,300);


// Create empty set for nodes and empty array for edges
let nodeIDlist = new Set();
let edgelist = [];
let nodelist = [];

// Create empty sets for nodes and edges to use as visjs data
let nodelistt = new DataSet()
let edgelistt = new DataSet()

// From here down the code should be able to run in batches, each nodedump is a batch

// Read in nodes
for(let i=0; i<nodedump.length;i++){
    let node = nodedump[i]
    nodelist.push(node)
    nodeIDlist.add(node.id)
}

// Read in edges
for(let i=0; i<nodedump.length; i++){
    let node = nodedump[i];
    let nodeID = node.id;
    let toNodes = node.out_edge;
    let fromNodes = node.in_edge;

    for( let j=0; j<toNodes.length; j++){
        let toNodeID = toNodes[j].to_node;
        // We only want to add edges if the toNode is a node, which means that we don't add literal types
        if (nodeIDlist.has(toNodeID)) {
            if(node.class_type=="explicit"){
                edgelist.push({from : nodeID, to : toNodeID, color : "#FFFFFF", label : toNodes[j].label})
            }
            else{
                edgelist.push({from : nodeID, to : toNodeID, label : toNodes[j].label})
            }
        }
    }

    for( let j=0; j<fromNodes.length; j++){
        let fromNodeID = fromNodes[j].to_node.split("-")[0];
        console.log(fromNodeID) // That works!!
        // We only want to add edges if the toNode is a node, which means that we don't add literal types, none of these exist as nodes!
        if (nodeIDlist.has(fromNodeID)) {console.log("IN EDGES ARE ADDED!!!")} //
        if (nodeIDlist.has(fromNodeID)) {edgelist.push({from : fromNodeID, to : nodeID, color : "#ff0000"})}
    }

}

// Create array to loop over, hacky way
nodeIDlist = Array.from(nodeIDlist)

// Rewrite the nodes into the right format although it might already be in the correct format
for(let i=0; i<nodelist.length;i++){
    let kleur = ""
    if(nodelist[i].class_type == "explicit") { kleur = "#FFFFFF" }
    else{ kleur = "#ECF38D"}
    nodelist[i] = { 
        id:nodelist[i].id, 
        label:nodelist[i].label,
        class_type : nodelist[i].class_type,
        class_overlaps : nodelist[i].class_overlaps,
        properties : nodelist[i].properties,
        out_edge : nodelist[i].out_edge,
        in_edge : nodelist[i].in_edge,
        color : kleur
    }
}


// function to split subgraphs into batches
const chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []);

// k is batchsize
let k = 1;
let nodelistchunk = chunk(nodelist, k)
let edgelistchunk = chunk(edgelist, k)


var data = {nodes:nodelistt, edges:edgelistt}

// each batch is added with a delay
for (let i=0; i<Math.max(nodelistchunk.length, edgelistchunk.length); i++){

    let nodebatch = [];
    let edgebatch = [];
    if(i<nodelistchunk.length){    nodebatch = nodelistchunk[i];}
    if(i<edgelistchunk.length){    edgebatch = edgelistchunk[i];}

    setTimeout(function() {
        nodebatch.forEach(node => nodelistt.add({
            id:node.id, 
            label: node.label, 
            color : {background : node.color, border : node.color},
            class_type : node.class_type,
            class_overlaps : node.class_overlaps,
            properties : node.properties,
            out_edge : node.out_edge,
            in_edge : node.in_edge
        }))
        edgebatch.forEach(edge => edgelistt.add(edge))
    }, 3000);
}


// Initializing the network
const VisNetwork = () => {

	const visJsRef = useRef(null);
	useEffect(() => {

        const options = {
            'interaction':{'hover':true},

            'layout':{
                'randomSeed': 1,
                // 'hierarchical':{
                //     'direction': 'DU',
                //     'sortMethod' : 'directed',
                //     'nodeSpacing' : 20,
                //     'treeSpacing' : 20,
                // }
            },


            'physics': {
                'enabled' : true,
                'stabilization' : false,
                "solver" : "barnesHut",
            },
            
            'edges': {
                'arrows' : 'to',
                'smooth' : true,
                'color': {
                    'color': '#ECF38D',
                    'hover': '#ECF38D',
                    'highlight': '#ECFF00'
                        },
            },
            'nodes': {
               // 'shape': "dot",
                'shapeProperties': {
                    'interpolation': false,    // 'true' for intensive zooming
                },
                'color': {
                    'background': '#ECF38D', 
                    'hover': '#ECF38D',
                    'highlight': '#ECFF00',
                    'border': '#ECF38D',
                }
            }
        }
		const network =
			visJsRef.current &&
			new Network(visJsRef.current, data, options );

        
        network.setSize(window.innerWidth, window.innerHeight);

        //window.onresize = function() {network.fit();}

        // network.on( 'hoverNode', function(properties) {
        //     var ids = properties.nodes;
        //     //var clickedNodes = nodelist.get(ids);

        //     // clickedNodes.forEach(node => {
        //     //     console.log('clicked node ' + node.prp.join());
        //     //     console.log(triple_store)
        //     // });
        // });

        network.on("click", (properties) => {
            var ids = properties.nodes;
            var clickedNodes = nodelistt.get(ids)[0];
            let out_edges_ids = "";
            let in_edges_ids = "";
            let class_overlaps_joined = clickedNodes.class_overlaps.join(", ")
            for (let i = 0; i<clickedNodes.out_edge.length ; i++){
                out_edges_ids = out_edges_ids + ", " + clickedNodes.out_edge[i].to_node
            }
            for (let i = 0; i<clickedNodes.in_edge.length ; i++){
                in_edges_ids = in_edges_ids + " " + clickedNodes.in_edge[i].to_node
            }
            if (in_edges_ids == "") {in_edges_ids = "None"}
            if (class_overlaps_joined == "") {class_overlaps_joined = "None"}
            window.alert("Class name:     " + clickedNodes.label + " \n " + 
                "Class properties:     " + clickedNodes.properties.join(", ") + "\n" + 
                "This is an     " + clickedNodes.class_type + "     class \n" + 
                "Overlaps with classes:     " + class_overlaps_joined + "\n" +
                "Outgoing edges to classes:     " + out_edges_ids + "\n" + 
                "Incoming edges from classes:     " + in_edges_ids
                )
        })


        // network.on( 'hover', function(properties) {
        //     var ids = properties.nodes;
        //     var clickedNodes = nodes.get(ids);

        //     clickedNodes.forEach(node => {
        //         console.log('hovered node ' + node.prp.join());
        //     });

        nodelistt.on("add",() => network.fit())

        //nodelistt.on("click", (nodeI) => window.alert(nodeI))
        // network.on("click", (clicknode) => {
        //     console.log("The click works!")
        // })
            
       
	}, [visJsRef, nodelistt, edgelistt]);

	return <div ref={visJsRef} />;
};

export default VisNetwork;


