class SchemaEdge{
    constructor(id, label, to_node){
        this.id = id
        this.label = label
        this.to_node = to_node
    }
}

export {SchemaEdge}