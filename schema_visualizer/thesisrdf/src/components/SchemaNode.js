
class SchemaNode {
    constructor(id, label, class_type, properties, freq, in_degree, out_degree){
        this.id = id
        this.class_type = class_type
        this.label = label
        this.properties = properties
        this.freq = freq
        this.in_degree = in_degree
        this.out_degree = out_degree
    }
}

export {SchemaNode}