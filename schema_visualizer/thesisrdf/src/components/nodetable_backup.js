import React, { useEffect, useRef, useLayoutEffect, useState } from "react";
import {network, Network, Node} from "vis-network";
import {DataSet, Item} from 'vis-data';
import { SchemaNode } from "./SchemaNode";
import { SchemaEdge } from "./SchemaEdge";

import hierarchyData from './hierarchyData.json'
import dataDump1 from './dataDump1.json'



const nodedump = dataDump1
const triple_store = hierarchyData["results"]["bindings"]

let nnnnodelist = new Set();
let edgelist = [];



nodedump.forEach(node => {
    let f = node.id;
    if(!nnnnodelist.has(f)){
        nnnnodelist.add(f)
        //console.log("node" + f)
    }

    // node.out_degree.forEach(e => {
    //     let t = e.to_node
    //     if(!nnnnodelist.has(t)){
    //         nnnnodelist.add(t)
    //         edgelist.push({from: f, to: t})
    //     }
        
        //console.log("edge from " + f + "to " + t)

    // })
})


nodedump.forEach(node => {

    let f = node.id;
    node.out_degree.forEach(e => {
        let t = e.to_node
        if(nnnnodelist.has(t)){
            edgelist.push({from: f, to: t})
        }
        
        //console.log("edge from " + f + "to " + t)

    })
})




// triple_store.forEach(triple => {
//     let f = triple.o.value.split("/").pop();
//     let t = triple.s.value.split("/").pop();
//     edgelist.push({from: f, to: t})
//     if(!nnnnodelist.has(f)){
//         nnnnodelist.add(f)
//     }
//     if(!nnnnodelist.has(t)){
//         nnnnodelist.add(t)
//     }
//  });

let nodelist = Array.from(nnnnodelist)
for(let i=0; i<nodelist.length;i++){
    nodelist[i] = {id:nodelist[i], label:nodelist[i]}
}

console.log("ALLE DATA IS INGELEZEN")

const chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []);


let k = 1;
let nodelistchunk = chunk(nodelist, k)
let edgelistchunk = chunk(edgelist, k)

let nodelistt = new DataSet()
let edgelistt = new DataSet()

// nodelistt.on("add",() => network.fit())

// for (let i=0; i<Math.max(nodelistchunk.length, edgelistchunk.length); i++){

//     let nodebatch = [];
//     let edgebatch = [];
//     if(i<nodelistchunk.length){    nodebatch = nodelistchunk[i];}
//     if(i<edgelistchunk.length){    edgebatch = edgelistchunk[i];}

//     setTimeout(function() {
//         nodebatch.forEach(node => nodelistt.add(node));
//         edgebatch.forEach(edge => edgelistt.add(edge))
//     }, 500);
// }


// // Examples
// console.log(chunk([1, 2, 3, 4, 5, 6, 7, 8], 3))     // [[1, 2, 3], [4, 5, 6], [7, 8]]
// console.log(chunk([1, 2, 3, 4, 5, 6, 7, 8], 4))


// let nodelistt = new DataSet(nodelist.slice(0,10))
// let edgelistt = new DataSet(edgelist.slice(0,10))



// for (let i=10; i<edgelist.length; i++){

//     setTimeout(function() {
//         if (i<nodelist.length){
//             nodelistt.add(nodelist[i]);
//     }
//         edgelistt.add(edgelist[i]);
//     }, 3000);
// }



var data = {nodes:nodelistt, edges:edgelistt}

const VisNetwork = () => {

	const visJsRef = useRef(null);
	useEffect(() => {
        // const options = {
        //     'interaction': {
        //         'hover': true,
        //     },
        //     'nodes': {
        //         'scaling': {
        //             'min': 10,
        //             'max': 30,
        //         },
        //         'color': {
        //             'border':'#07558c',
        //             'background':'#55b5fa'
        //         }
        //     },
        //     'edges': {
        //         'arrows': 'to',
        //         'smooth': true,
        //     },
        //     'layout': {
        //         'improvedLayout': false
        //     },
        //     'physics': {
        //         'enabled': true,
        //         'stabilization': {
        //             'enabled': true,
        //             'iterations': 1000
        //         },
        //         'solver': 'forceAtlas2Based'
        //     }
        // }

        const options = {
            'layout':{
                'randomSeed': 1,
                'hierarchical':{
                    'direction': 'DU',
                    'sortMethod' : 'directed',
                    'nodeSpacing' : 20,
                    'treeSpacing' : 20,
                }
            },

            'physics': {
                'enabled' : true,
                'stabilization': {
                                'enabled': true,
                                'iterations': 1000
                            },
            },
            
  
            //width: (window.innerWidth - 200) + "px",
            //height: (window.innerHeight - 200) + "px",

            //'display': 'block',
            //'solver': 'forceAtlas2Based',

            //'interaction': { 'dragNodes': true},

            'edges': {
                'arrows' : 'to',
                'smooth' : true,
                'color': {
                    'color': '#F0FF33',
                    'hover': '#ff6e63',
                    'highlight': '#ff6e63'
                        },
            },
            'nodes': {
                'shapeProperties': {
                    'interpolation': false,    // 'true' for intensive zooming
                },
                // 'interaction' : {
                //     hover: true
                // },
                'color': {
                    'background': '#F0FF33',
                    'hover': '#ff6e63',
                    'highlight': '#ff6e63'
                }
              }
        }
		const network =
			visJsRef.current &&
			new Network(visJsRef.current, data, options );

        
        network.setSize(window.innerWidth, window.innerHeight);
        //window.onresize = function() {network.fit();}

        // network.on( 'click', function(properties) {
        //     var ids = properties.nodes;
        //     var clickedNodes = nodelist.get(ids);

        //     clickedNodes.forEach(node => {
        //         console.log('clicked node ' + node.prp.join());
        //         console.log(triple_store)
        //     });

            
        
        // });

        // network.on( 'hover', function(properties) {
        //     var ids = properties.nodes;
        //     var clickedNodes = nodes.get(ids);

        //     clickedNodes.forEach(node => {
        //         console.log('hovered node ' + node.prp.join());
        //     });

        nodelistt.on("add",() => network.fit())
            
       
	}, [visJsRef, nodelist, edgelist]);


for (let i=0; i<Math.max(nodelistchunk.length, edgelistchunk.length); i++){

    let nodebatch = [];
    let edgebatch = [];
    if(i<nodelistchunk.length){    nodebatch = nodelistchunk[i];}
    if(i<edgelistchunk.length){    edgebatch = edgelistchunk[i];}

    setTimeout(function() {
        nodebatch.forEach(node => nodelistt.add(node));
        edgebatch.forEach(edge => edgelistt.add(edge))
    }, 10);
}
	return <div ref={visJsRef} />;
};

export default VisNetwork;


