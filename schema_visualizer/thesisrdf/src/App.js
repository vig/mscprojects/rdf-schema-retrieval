
import React from 'react';
import { Popup } from 'vis-util/esnext';
import './App.css';
import VisNetwork from './components/nodeTable';

  	

function App() {
  return (
    <div className="App">
      <VisNetwork></VisNetwork>
    </div>
  );


}

export default App;
