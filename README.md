# <img src="logo.png"> GraphPolaris

# Master Thesis Projects 
Supervised by Michael Behrisch
- Partial RDF Schema Retrieval
- Connecting the dots... A step towards Linked Open Data by incrementally visualizing an emergent RDF schema

<br/>

## Partial RDF Schema Retrieval [[source code]](https://git.science.uu.nl/vig/mscprojects/rdf-schema-retrieval/-/tree/main/partial_rdf_schema_retrieval) :eyes:

### Author
Koray Poyraz

### Abstract
There are various data structures that represent data interrelationships in the universe of information. One is a graph-based data structure, which depicts a collection of entities connected by relationships. Resource Description Framework (RDF) is a widely used data model that facilitates the storage of graph-based data. This system, unlike standardised SQL, lacks a consistent schema that evolves over time. When presenting a complete schema is crucial, the loose standards combined with timeout limits in the retrieval process pose a challenge. The objective of this master's thesis is therefore to develop a partial schema retrieval pipeline in order to solve the previously outlined problem. We evaluate the quality of our approach by measuring performance and completeness. This is conducted by running the pipeline against several SPARQL-endpoints. The pipeline lays the foundation for retrieving partial graph schemas per iteration. The result is a rendered set of visualisations of partial schemas displayed in a hierarchical aggregated view. This should provide the ability to iteratively express portion of a graph, regardless of the evolving schema.

<br/>

## Connecting the dots... A step towards Linked Open Data by incrementally visualizing an emergent RDF schema [[source code]](https://git.science.uu.nl/vig/mscprojects/rdf-schema-retrieval/-/tree/main/schema_visualizer/thesisrdf) :eyes:
### Author
Irma Mastenbroek

### Abstract
Graphs are a popular way to store and visualize information. In the era of big data, the need for scalable and interactive visualization tools rises. In the Linked Open Data cloud, objects, events or concepts are described to have some pre-defined relationship to other entities. In a purely theoretical sense, one could create one huge knowledge graph of all interrelations of all available information known to man. However, due to historical reasons not all graph data structures are consistent and the visualization techniques for graphs do not scale up well. In this thesis, we will focus on emerging a schema of data stored in a Resource Description Framework (RDF) format. An example of data stored in RDF manner is DBpedia, a project aiming to extract all structured content created on Wikipedia. The retrieval of specific facts from an RDF dataset is often hindered by the lack of schema knowledge, further complicated by noisy data, annotation mistakes and time-outs when queries are too demanding. A schema can be seen as a clustering of the RDF-triples, downscaling the graph making interpretation and visualization possible. In this research we create an interactive node-link visualization of the global schema that enables users to discover the schema of any big RDF dataset. The tool is capable of iteratively expanding the graph as new parts of the graph arrive from the backend.